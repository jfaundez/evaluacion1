<%-- 
    Document   : index
    Created on : 22-09-2020, 23:57:07
    Author     : Jean Carlos Faundez
--%>



<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Calculadora de interés simple con periodos anuales</h1>


        <form name="form" action="CalculadoraServlet" method="post">
            <table border="1px">
                <tr>
                    <td>Ingrese capital</td>
                    <td><input type="text" name="txtCapital" placeholder="Ingrese capital" required=""/></td>
                </tr>
                <tr>
                    <td>Tasa de Interes Anual</td>
                    <td><input type="text" name="txtInteres" placeholder="Tasa interes" required=""/></td>
                </tr>
                <tr>
                    <td>Ingrese numero de años</td>
                    <td><input type="text" name="txtTiempo" placeholder="Años" required=""/></td>
                </tr>
                <tr style="text-align: right;">
                    <td colspan="2"><input type="submit" name="btnCalcular" value="Calcular" required=""/></td>
                </tr>
            </table>
        </form>


    </body>
</html>
